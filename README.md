# AdSD coding exercises: SQL (Structured Query Language)

Dit repository bevat een SQLite database met fictieve voorbeelddata (meer info over de dataset: http://2016.padjo.org/tutorials/sql-simplefolks-overview/) en een eenvoudige [Node.js](https://nodejs.org/en/) applicatie die verbinding maakt met de database. Tevens bevat ze een reeks unit tests voor elk van de oefeningen. Elke test case bevat een korte omschrijving (bv. 'Select all the data from the people'). Probeer voor elke test de correcte SQL query te schrijven om de test te laten slagen. Bij de complexere queries zijn er meerdere antwoorden mogelijk.

1. Clone het repository naar je computer: (`git clone https://gitlab.com/adsd-hu/sql-oefeningen.git`)
2. Installeer de dependencies met npm: `npm install`.
3. Open het bestand `test.js` en vul de queries in.
4. Voer de unit tests uit met het commando `npm test`.

Dependencies:
- [Node.js](https://nodejs.org/en/)

Meer informatie:
- [SQLite Simple Folks: Overview ](http://2016.padjo.org/tutorials/sql-simplefolks-overview/)